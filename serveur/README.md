# Serveur
L'api est servie en PHP. Par commodité, l'extension `.php` n'est pas nécessaire grâce à l'extension Apache **RewriteEngine**

## Dépendences
 * Google APIclient (Composer) pour authentification

## API REST

 * `/items`
   * **GET**: liste des objets, hors-service exclus
   * **POST**: création d'un item

 * `/item/{id}`
   * **GET**: informations sur l'objet (identique à la liste) plus la liste des réservations futures
   * **DELETE**: suppression d'un objet (toujours présent dans la base, mis hors-service) **admin uniquement**
   * **PUT**: mise à jour des informations **admin uniquement**
   * **POST**: réservation
