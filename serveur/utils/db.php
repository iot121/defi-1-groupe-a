<?php

$NOT_FOUND = -1;
$UNAVAILABLE = -2;
$MISSING_DATA = -3;
$INVALID_DATA = -4;

// Match dates between 2000-01-01 and 2199-12-31, with optional time
$date_pattern = '/^2[01]\d\d-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01])( [0-2]\d(:[0-5]\d){2})?$/';

function db_connect() {
  $db = new mysqli("p:mysql-aerath.alwaysdata.net", "aerath_iot", "mdp_iot", "aerath_iot");
  if($db->connect_error)
    die("MySQL connection error ($db->connect_errno): $db->connect_error");
  return $db;
}

?>
