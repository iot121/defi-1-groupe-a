<?php
require_once('vendor/autoload.php');
require_once('db.php');

$CLIENT_ID = '779196148004-k6bh87vgfj5jgrbif7tal63tdh9i5d3f.apps.googleusercontent.com';

function endsWith($str, $substr) {
  $len = strlen($substr);

  return substr($str, -$len) === $substr;
}

function isAdmin($email) {
  $db = db_connect();

  $stmt = $db->prepare('SELECT NULL FROM users WHERE user=? AND isAdmin=1');
  $stmt->bind_param("s", $email);
  $stmt->execute();

  $res = $stmt->get_result();
  $row = $res->fetch_assoc();

  return $row != false;
}

function verifyToken($id_token) {
	global $INVALID_DATA;

  $client = new Google_Client(['client_id' => $CLIENT_ID]);
  $payload = $client->verifyIdToken($id_token);

  if($payload) {
    $domain = $payload['hd'];
    if(endsWith($domain, "icam.fr"))
      return $payload['email'];
  }
  return $INVALID_DATA;
}

function getUser() {
	global $INVALID_DATA, $MISSING_DATA;

  if(!isset($_SERVER['Authorization']))
    return $MISSING_DATA;

  $header = trim($_SERVER["Authorization"]);
  if(!preg_match('/Bearer \S+$/', $header))
    return $INVALID_DATA;

  return verifyToken(substr($header, 7));
}
?>
