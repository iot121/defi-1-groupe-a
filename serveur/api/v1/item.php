<?php

require_once('../../utils/db.php');
require_once('../../utils/auth.php');

function product_by_id($id, $get_admin_info=false) {
  global $NOT_FOUND, $UNAVAILABLE;
  global $date_pattern;

  $db = db_connect();

  if($get_admin_info)
    $stmt = $db->prepare('SELECT * FROM items WHERE id=?');
  else
    $stmt = $db->prepare('SELECT id, name, supplier, location, documentation, numeric_documentation FROM items WHERE id=?');
  $stmt->bind_param('d', $id);
  $stmt->execute();

  $res = $stmt->get_result();
  $product = $res->fetch_assoc();
  $product['borrowings'] = [];

  if($product === null)
    return $NOT_FOUND;

  $stmt = $db->prepare('SELECT start, end, borrower FROM borrowings WHERE item=? AND end > CURDATE()');
  $stmt->bind_param('d', $id);
  $stmt->execute();
  $res = $stmt->get_result();
  while($row = $res->fetch_assoc())
    $product['borrowings'] []= $row;

  return $product;
}

function borrow($id, $start, $end, $borrower, $user) {
  global $NOT_FOUND, $UNAVAILABLE, $MISSING_DATA, $INVALID_DATA;

  if(empty($start) || empty($end))
    return $MISSING_DATA;

  if(empty($borrower))
    $borrower = $user;

  // Match dates between 2000-01-01 and 2199-12-31, with optional time
  $date_pattern = '/^2[01]\d\d-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01])( [0-2]\d(:[0-5]\d){2})?$/';

  if(!preg_match($date_pattern, $start)
      || !preg_match($date_pattern, $end))
    return $INVALID_DATA;

  $db = db_connect();

  // Check availability
  // If item exists but no borrowings overlaps with $start-$end, we get one row with NULL start
  // If item exists and at least on borrowing overlaps with $start-$end, we get one row with start date
  // If item doesn't exists no $row will match
  $stmt = $db->prepare('SELECT start FROM items LEFT JOIN borrowings ON items.id=item AND (? BETWEEN start AND end OR ? BETWEEN start AND end) WHERE items.id=?');
  $stmt->bind_param('ssd', $start, $end, $id);
  $stmt->execute();
  $res = $stmt->get_result();
  $row = $res->fetch_assoc();

  if($row === null)
    return $NOT_FOUND;

  if($row['start'] != null)
    return $UNAVAILABLE;

  $stmt = $db->prepare('INSERT INTO borrowings(item, start, end, borrower, user) VALUES(?, ?, ?, ?, ?)');
  $stmt->bind_param('dssss', $id, $start, $end, $borrower, $user);
  $res = $stmt->execute();

  if($res === false)
    return $INVALID_DATA;

  return true;
}

function update_product($id, $product) {
  global $NOT_FOUND, $UNAVAILABLE, $MISSING_DATA, $INVALID_DATA;

  if(
        empty($product->name)
      || empty($product->supplier)
      //|| empty($product->location)
      //|| empty($product->documentation)
      //|| empty($product->numeric_documentation)
      //|| empty($product->state)
      || empty($product->bought) && $product->bought != 0
      || empty($product->amortization) && $product->amortization != 0
      //|| empty($product->cost)
      //|| empty($product->maintenance)

      || !is_int($product->bought)
      || !is_int($product->amortization)
      || !empty($product->cost) && !is_int($product->cost)
      || !empty($product->maintenance) && !is_int($product->maintenance)
  )
    return $MISSING_DATA;

  $db = db_connect();
  $stmt = $db->prepare('UPDATE items SET name=?, supplier=?, location=?, documentation=?, numeric_documentation=?, state=?, bought=?, amortization=?, cost=?, maintenance=? WHERE id=?');
  $stmt->bind_param('ssssssddddd', $product->name, $product->supplier, $product->location, $product->documentation, $product->numeric_documentation, $product->state, $product->bought, $product->amortization, $product->cost, $product->maintenance, $id);
  $res = $stmt->execute();

  if($res === false)
    return $INVALID_DATA;

  if($stmt->affected_rows != 1)
    return $NOT_FOUND;

  return true;
}

function delete_product($id) {
  global $NOT_FOUND, $UNAVAILABLE;

  $db = db_connect();

  $stmt = $db->prepare('UPDATE items SET removed=1 WHERE id=?');
  $stmt->bind_param('d', $id);
  $stmt->execute();

  if($stmt->affected_rows != 1)
    return $NOT_FOUND;
  return true;
}

$user = getUser();
if(!is_string($user)) {
  http_response_code(401);
  exit();
}

$method = $_SERVER['REQUEST_METHOD'];
$id = $_GET['id'];

if(empty($id)) {
  http_response_code(400);
  exit();
}

if($method === 'GET') {
  global $NOT_FOUND, $UNAVAILABLE;

  $product = product_by_id($id, isAdmin($user));
  if($product === $NOT_FOUND) {
    http_response_code(404);
    exit();
  }
  echo(json_encode($product));
  exit();
}

if($method === 'POST') {
  $body = json_decode(file_get_contents('php://input'));
  $start = $body->start;
  $end = $body->end;
  $borrower = $body->borrower;
  $res = borrow($id, $start, $end, $borrower, $user);

  if($res >= 0)
    exit();

  if($res === $NOT_FOUND)
    http_response_code(404);
  else if($res === $UNAVAILABLE)
    http_response_code(409);
  else if($res === $MISSING_DATA)
    http_response_code(400);
  else if($res === $INVALID_DATA)
    http_response_code(422);

  exit();
}

if($method === 'PUT' || $method === 'DELETE') {
  if(!isAdmin($user)) {
    http_response_code(403);
    exit();
  }

  if($method === 'DELETE') {
    //TODO: check it's not currently borrowed
    $res = delete_product($id);
  }
  if($method === 'PUT') {
    $body = json_decode(file_get_contents('php://input'));
    $res = update_product($id, $body);
  }

  if($res === $NOT_FOUND)
    http_response_code(404);
  else if($res === $MISSING_DATA)
    http_response_code(400);
  else if($res === $INVALID_DATA)
    http_response_code(422);

  exit();
}

http_response_code(405);

?>
