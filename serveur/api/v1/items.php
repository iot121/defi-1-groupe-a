<?php

require_once('../../utils/db.php');
require_once('../../utils/auth.php');

function products($get_admin_info=false) {
  $db = db_connect();

  if($get_admin_info)
    $res = $db->query('SELECT * FROM items WHERE removed != 1');
  else
    $res = $db->query('SELECT id, name, supplier, location, documentation, numeric_documentation FROM items WHERE removed != 1');

  while($row = $res->fetch_assoc())
    $products[] = $row;

  return $products;
}

function products_search($search, $get_admin_info=false) {
  $db = db_connect();

  if($get_admin_info)
    $stmt = $db->prepare('SELECT * FROM items WHERE removed != 1 AND (name LIKE ? OR supplier LIKE ?)');
  else
    $stmt = $db->prepare('SELECT id, name, supplier, location, documentation, numeric_documentation FROM items WHERE removed != 1 AND (name LIKE ? OR supplier LIKE ?)');

  $stmt->bind_param('ss', $search, $search);
  $stmt->execute();
  $res = $stmt->get_result();

  while($row = $res->fetch_assoc())
    $products[] = $row;

  return $products;
}

function create_product($product) {
  global $MISSING_DATA, $INVALID_DATA;

  if(
       empty($product->name)
    || empty($product->supplier)
    //|| empty($product->location)
    || empty($product->amortization) && $product->amortization != 0

    || !empty($product->amortization) && !is_int($product->amortization)
  )
    return $MISSING_DATA;

  $db = db_connect();
  $stmt = $db->prepare('INSERT INTO items(name, supplier, location, bought, amortization) VALUES(?, ?, ?, YEAR(CURDATE()), ?)');
  $stmt->bind_param('sssi', $product->name, $product->supplier, $product->location, $product->amortization);
  // Non-select queries in prepared statements always return false in get_result
  $res = $stmt->execute();

  if($res === false)
    return $INVALID_DATA;

  return $db->insert_id;
}

$user = getUser();
if(!is_string($user)) {
  http_response_code(401);
  exit();
}

$method = $_SERVER['REQUEST_METHOD'];
$search = $_GET['search'];

if($method != 'GET' && $method != 'POST') {
  http_response_code(405);
  exit();
}

if(!empty($search) && $method != 'GET') {
  http_response_code(405);
  exit();
}

if(!empty($search)) {
  if($method === 'GET') {
    echo(json_encode(products_search($search)));
    exit();
  }
  http_response_code(405);
  exit();
}

if($method === 'GET') {
  echo(json_encode(products()));
  exit();
}

if($method === 'POST') {
  if(!isAdmin($user)) {
    http_response_code(403);
    exit();
  }

  $body = json_decode(file_get_contents('php://input'));
  $created_product = create_product($body);

  if($created_product >= 0)
    echo($created_product);
  else if($created_product === $MISSING_DATA)
    http_response_code(400);
  else if($created_product === $INVALID_DATA)
    http_response_code(422);

  exit();
}

http_response_code(400);

?>
