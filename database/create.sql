CREATE TABLE `items` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`name` TINYTEXT NOT NULL COLLATE 'utf8mb4_general_ci',
	`supplier` TINYTEXT NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`location` TINYTEXT NULL DEFAULT NULL COMMENT 'Fixed ?' COLLATE 'utf8mb4_general_ci',
	`documentation` TINYTEXT NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
	`numeric_documentation` TINYTEXT NULL DEFAULT NULL COMMENT 'Link ?' COLLATE 'utf8mb4_general_ci',
	`state` TINYTEXT NULL DEFAULT NULL COMMENT ' ???' COLLATE 'utf8mb4_general_ci',
	`bought` YEAR NULL DEFAULT NULL,
	`amortization` SMALLINT(5) UNSIGNED NOT NULL,
	`cost` MEDIUMINT(8) UNSIGNED NULL DEFAULT NULL,
	`maintenance` MEDIUMINT(8) UNSIGNED NULL DEFAULT NULL,
	`removed` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=552
;

CREATE TABLE `borrowings` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`item` INT(10) UNSIGNED NOT NULL,
	`start` DATE NOT NULL,
	`end` DATE NOT NULL,
	`borrower` CHAR(50) NOT NULL COMMENT 'designation of the person or organization unit who will use it' COLLATE 'utf8mb4_general_ci',
	`user` CHAR(50) NOT NULL COMMENT 'account who registered this' COLLATE 'utf8mb4_general_ci',
	PRIMARY KEY (`id`) USING BTREE,
	INDEX `FK__items` (`item`) USING BTREE,
	CONSTRAINT `FK__items` FOREIGN KEY (`item`) REFERENCES `aerath_iot`.`items` (`id`) ON UPDATE RESTRICT ON DELETE RESTRICT
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=13
;

CREATE TABLE `users` (
	`user` CHAR(50) NOT NULL COMMENT 'email' COLLATE 'utf8mb4_general_ci',
	`isAdmin` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY (`user`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;
