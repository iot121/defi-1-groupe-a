# Base de données

## Utilisateurs
Liste d'utilisateurs par email avec le statut administrateur

Chaque utilisateur n'a pas besoin d'être présent (utilisation de la base utilisateur Google), seulement ceux administrateurs

## Objets
Liste des objets y compris ceux non disponibles (HS, jetés etc)

## Réservations
Liste de réservations (intervalle en jours) par un utilisateur (email) au nom d'une personne (champ libre)
