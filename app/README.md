# Application Android

## Usage
 * Ouverture de l'app
 	 1. Affichage de l'UI de connection si non connecté
 	 1. Actualisation des identifiants
 	 1. Récupération des objets
 * Consultation
   * Filtrage  par mots (dans nom ou fournisseur)
   * Filtrage par favoris (combinable à la recherche)
   * Intégration des PDFs de documentations numériques depuis le Drive
 * Affichage de la vue "Scan QR"
 	 1. Téléchargement du composant vision des Play Services si absents (nécessite un accès internet la première fois)
   1. Recherche d'un tag au format `reservicam://id`
	 1. Affichage des infos récupérées précédemment pour l'item correspondant à l'id
 * Réservation
   1. Sélection d'un objet dans la liste
   1. Bouton réservation
   1. Choix de la date de début (affiche également les dates non disponibles)
   1. Choix de la date de fin

## Dépendances
* Application Android
  * Android 4.1 (API 16)
  * Play Services (auth+vision)
  * Retrofit (REST)
  * MaterialDatetimePicker
