package fr.icam.i121.iot.reservicam.api;

import android.Manifest;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresPermission;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APISpecification {
	@GET("items")
	@RequiresPermission(Manifest.permission.INTERNET)
	Call<List<Item>> getItems(@Query("search") @Nullable String search);

	@POST("items")
	@RequiresPermission(Manifest.permission.INTERNET)
	Call<Integer> createItem(@Body @NonNull Item item);

	@GET("item/{id}")
	@RequiresPermission(Manifest.permission.INTERNET)
	Call<Item> getItem(@Path("id") @IntRange(from = 0) int id);

	@PUT("item/{id}")
	@RequiresPermission(Manifest.permission.INTERNET)
	Call<Void> updateItem(@Path("id") @IntRange(from = 0) int id, @NonNull @Body Item item);

	@DELETE("item/{id}")
	@RequiresPermission(Manifest.permission.INTERNET)
	Call<Void> deleteItem(@Path("id") @IntRange(from = 0) int id);

	@POST("item/{id}")
	@RequiresPermission(Manifest.permission.INTERNET)
	Call<Void> borrowItem(@Path("id") @IntRange(from = 0) int id, @NonNull @Body Borrow borrow);
}
