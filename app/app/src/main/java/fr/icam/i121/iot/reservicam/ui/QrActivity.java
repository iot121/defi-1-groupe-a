package fr.icam.i121.iot.reservicam.ui;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresPermission;
import androidx.annotation.UiThread;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

import fr.icam.i121.iot.reservicam.R;
import fr.icam.i121.iot.reservicam.databinding.ActivityQrBinding;

public class QrActivity extends AppCompatActivity implements Detector.Processor<Barcode> {
	private static final int REQUEST_CAMERA_PERMISSION = 200;
	private static final int REQUEST_PLAY_SERVICES_RESOLUTION = 300;

	private BarcodeDetector detector;
	private CameraSource camera;

	private Barcode lastSeenBcode;

	private ActivityQrBinding binding;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		binding = ActivityQrBinding.inflate(getLayoutInflater());
		setContentView(binding.getRoot());

		checkPlayServices();

		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this, new String[]{ Manifest.permission.CAMERA }, REQUEST_CAMERA_PERMISSION);
		}

		detector = new BarcodeDetector.Builder(this).setBarcodeFormats(Barcode.QR_CODE).build();
		detector.setProcessor(this);

		if (detector.isOperational())
			binding.buttonScan.setEnabled(true);
		else {
			new AlertDialog.Builder(this)
					.setTitle(R.string.error)
					.setMessage(R.string.error_detector_not_operational)
					.setCancelable(true)
					.create().show();
		}
	}

	@UiThread
	@RequiresPermission(Manifest.permission.CAMERA)
	public void startScan(View view) throws IOException {
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
			binding.buttonScan.setEnabled(false);
			return;
		}

		Resources resources = getResources();
		CameraSource.Builder builder = new CameraSource.Builder(getApplicationContext(), detector)
				.setFacing(CameraSource.CAMERA_FACING_BACK)
				.setRequestedPreviewSize((int) resources.getDimension(R.dimen.cameraPreview_height), (int) resources.getDimension(R.dimen.cameraPreview_width))
				.setRequestedFps(15.0f);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
			builder = builder.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);

		camera = builder.build();
		camera.start(binding.surfaceView.getHolder());
	}

	private boolean checkPlayServices() {
		GoogleApiAvailability gApi = GoogleApiAvailability.getInstance();
		int resultCode = gApi.isGooglePlayServicesAvailable(this);
		if (resultCode == ConnectionResult.SUCCESS)
			return true;

		if (gApi.isUserResolvableError(resultCode))
			gApi.getErrorDialog(this, resultCode, REQUEST_PLAY_SERVICES_RESOLUTION).show();
		return false;
	}

	@Override
	public void release() {
	}

	@Override
	public void receiveDetections(@NonNull Detector.Detections<Barcode> detections) {
		SparseArray<Barcode> barcodes = detections.getDetectedItems();

		if (barcodes.size() == 0) return;
		lastSeenBcode = barcodes.valueAt(0);

		runOnUiThread(() -> binding.textView.setText(lastSeenBcode.rawValue));

		if (lastSeenBcode.rawValue.matches("reservicam://\\d+")) {
			camera.stop();

			int id = Integer.parseInt(lastSeenBcode.rawValue.substring(13));
			startActivity(ItemDetailActivity.getStartIntent(this, id));
		}
	}
}
