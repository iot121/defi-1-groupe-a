package fr.icam.i121.iot.reservicam.api;

import androidx.annotation.NonNull;

import java.io.IOException;

import fr.icam.i121.iot.reservicam.App;
import okhttp3.Authenticator;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

class IdTokenAuthenticator implements Authenticator, Interceptor {
	@Override
	public Request authenticate(Route route, @NonNull okhttp3.Response response) {
		String token = App.GoogleAuth.getIdTokenSync();
		if (token == null)
			return null;

		boolean isPreemptive = response.challenges().stream()
				.anyMatch(chal -> chal.scheme().equalsIgnoreCase("OkHttp-Preemptive"));
		if (isPreemptive)
			return response.request().newBuilder()
					.header("Authorization", "Bearer " + token)
					.build();

		if (response.request().header("Authorization") != null)
			return null; // Give up, we've already failed to authenticate.

		return response.request().newBuilder()
				.header("Authorization", "Bearer " + token)
				.build();
	}

	@NonNull
	@Override
	public Response intercept(@NonNull Chain chain) throws IOException {
		Request original = chain.request();

		String token = App.GoogleAuth.getIdTokenSync();
		if (token == null)
			return chain.proceed(original);

		Request request = original.newBuilder()
				.addHeader("Authorization", "Bearer " + token)
				.build();
		return chain.proceed(request);
	}
}
