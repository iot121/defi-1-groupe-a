package fr.icam.i121.iot.reservicam;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.internal.EverythingIsNonNull;

public class LambdaCallback<T> extends DefaultCallback<T> {
	private final SuccessCallback<T> successCallback;

	public LambdaCallback(@NonNull View view, SuccessCallback<T> successCallback) {
		super(view);
		this.successCallback = successCallback;
	}

	public LambdaCallback(@NonNull View view, @StringRes int failPrefix, SuccessCallback<T> successCallback) {
		super(view, failPrefix);
		this.successCallback = successCallback;
	}

	public LambdaCallback(@NonNull View view, @StringRes int failPrefix, int snackDuration, SuccessCallback<T> successCallback) {
		super(view, failPrefix, snackDuration);
		this.successCallback = successCallback;
	}

	@Override
	@EverythingIsNonNull
	protected void onSuccess(Call<T> call, Response<T> response) {
		successCallback.onSuccess(call, response);
	}

	public interface SuccessCallback<T> {
		@EverythingIsNonNull
		void onSuccess(Call<T> call, Response<T> response);
	}
}
