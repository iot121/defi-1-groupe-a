package fr.icam.i121.iot.reservicam.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.UiThread;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.api.CommonStatusCodes;

import fr.icam.i121.iot.reservicam.App;
import fr.icam.i121.iot.reservicam.GoogleAuth;
import fr.icam.i121.iot.reservicam.R;
import fr.icam.i121.iot.reservicam.databinding.ActivityAuthBinding;

public class AuthActivity extends AppCompatActivity {
	private ActivityAuthBinding binding;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		binding = ActivityAuthBinding.inflate(getLayoutInflater());
		setContentView(binding.getRoot());

		// Not working from XML, as per Google Reference
		// https://developers.google.com/android/reference/com/google/android/gms/common/SignInButton
		// "Note that you must explicitly call setOnClickListener(OnClickListener).
		// Do not register a listener via XML, or you won't receive your callbacks."
		binding.buttonSignIn.setOnClickListener(view -> App.GoogleAuth.signIn(this));

		if (App.GoogleAuth.isConnected())
			onSignedIn();
		else onSignedOut();
	}

	@UiThread
	private void onSignedIn() {
		binding.buttonSignIn.setEnabled(false);
		binding.buttonSignOut.setEnabled(true);
		Toast.makeText(this, "Connecté en tant que " + App.GoogleAuth.getDisplayName(), Toast.LENGTH_SHORT).show();
	}

	@UiThread
	private void onSignedOut() {
		binding.buttonSignIn.setEnabled(true);
		binding.buttonSignOut.setEnabled(false);
		Toast.makeText(this, "Déconnecté", Toast.LENGTH_SHORT).show();
	}

	@UiThread
	private void onSignInFailed(int code) {
		switch (code) {
			case CommonStatusCodes.DEVELOPER_ERROR:
				// Caused by an invalid client ID, a non-signed APK or with a signature not matching
				// the one configured in Google Developer Console
				Toast.makeText(this, R.string.error_developper, Toast.LENGTH_SHORT).show();
				break;
			case CommonStatusCodes.NETWORK_ERROR:
				Toast.makeText(this, R.string.error_network, Toast.LENGTH_SHORT).show();
				break;
			case CommonStatusCodes.INTERRUPTED:
				break;
			case CommonStatusCodes.INVALID_ACCOUNT:
				Toast.makeText(this, R.string.error_invalid_account, Toast.LENGTH_SHORT).show();
				break;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == GoogleAuth.RC_SIGN_IN) {
			int resCode = App.GoogleAuth.onSignInResult(data);
			if (resCode == CommonStatusCodes.SUCCESS) {
				onSignedIn();
				finish();
			} else onSignInFailed(resCode);
		}
	}

	public void signIn(@Nullable View view) {
		App.GoogleAuth.signIn(this);
	}

	public void signOut(@Nullable View view) {
		App.GoogleAuth.signOut();
	}
}