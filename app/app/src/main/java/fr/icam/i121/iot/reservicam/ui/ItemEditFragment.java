package fr.icam.i121.iot.reservicam.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.material.appbar.CollapsingToolbarLayout;

import fr.icam.i121.iot.reservicam.R;
import fr.icam.i121.iot.reservicam.api.Item;
import fr.icam.i121.iot.reservicam.databinding.ItemEditBinding;

public class ItemEditFragment extends Fragment {
	private static final String ARGUMENT_ITEM_ID = "ARGUMENT_ITEM_ID";

	private Item mItem;
	private ItemEditBinding binding;

	public ItemEditFragment() {
		// Required empty public constructor
	}

	@NonNull
	static ItemEditFragment newInstance(@IntRange(from = Item.DEFAULT_ID) int itemID) {
		ItemEditFragment frag = new ItemEditFragment();

		if (itemID == Item.DEFAULT_ID)
			return frag;

		Bundle arguments = new Bundle();
		arguments.putInt(ARGUMENT_ITEM_ID, itemID);
		frag.setArguments(arguments);

		return frag;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		int itemID = Item.DEFAULT_ID;

		Bundle arguments = getArguments();
		if (getArguments() != null && arguments.containsKey(ARGUMENT_ITEM_ID))
			itemID = arguments.getInt(ARGUMENT_ITEM_ID);

		if (itemID == Item.DEFAULT_ID)
			mItem = new Item();
		else mItem = ItemListActivity.getItem(itemID);


		Activity activity = getActivity();
		CollapsingToolbarLayout appBarLayout = activity.findViewById(R.id.toolbar_layout);
		if (appBarLayout != null)
			appBarLayout.setTitle(mItem.name);
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		binding = ItemEditBinding.inflate(inflater, container, false);

		binding.inputName.getEditText().setText(mItem.name);
		binding.inputSupplier.getEditText().setText(mItem.supplier);
		binding.inputLocation.getEditText().setText(mItem.location);
		binding.inputDoc.getEditText().setText(mItem.documentation);
		binding.inputNumericDoc.getEditText().setText(mItem.numeric_documentation);
		binding.inputCost.getEditText().setText(String.format("%,d", mItem.cost));
		binding.inputAmortization.getEditText().setText(String.format("%,d", mItem.amortization));
		binding.inputMaintenance.getEditText().setText(String.format("%,d", mItem.maintenance));

		return binding.getRoot();
	}

	@NonNull
	Item getInput() {
		Item item = new Item();

		item.name = binding.inputName.getEditText().getText().toString();
		item.supplier = binding.inputSupplier.getEditText().getText().toString();
		item.location = binding.inputLocation.getEditText().getText().toString();
		item.documentation = binding.inputDoc.getEditText().getText().toString();
		item.numeric_documentation = binding.inputNumericDoc.getEditText().getText().toString();
		item.amortization = Integer.parseInt(binding.inputAmortization.getEditText().getText().toString());
		item.cost = Integer.parseInt(binding.inputCost.getEditText().getText().toString());
		item.maintenance = Integer.parseInt(binding.inputMaintenance.getEditText().getText().toString());

		return item;
	}
}
