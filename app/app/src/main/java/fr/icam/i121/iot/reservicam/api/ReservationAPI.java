package fr.icam.i121.iot.reservicam.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import fr.icam.i121.iot.reservicam.BuildConfig;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class ReservationAPI {
	private final static String API_ROOT = "https://iot.aerath.tk/serveur/api";
	private final static String API_VERSION = "v1";

	public final static APISpecification api;

	static {
		OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder()
				.addInterceptor(new IdTokenAuthenticator())
				.authenticator(new IdTokenAuthenticator());

		//TODO: find a way to remove specific lines of code in release build
		// Currently still compiled (or rather, attempted to be) in release, despite being unreachable
		if(BuildConfig.DEBUG)
			clientBuilder.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));

		OkHttpClient client = clientBuilder.build();

		Gson gson = new GsonBuilder()
				.setDateFormat("yyyy-MM-dd HH:mm:ss")
				.create();

		Retrofit Retrofit = new Retrofit.Builder()
				.baseUrl(API_ROOT + '/' + API_VERSION + '/')
				.client(client)
				.addConverterFactory(GsonConverterFactory.create(gson))
				.build();

		api = Retrofit.create(APISpecification.class);
	}
}