package fr.icam.i121.iot.reservicam.api;


import androidx.annotation.IntRange;
import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import fr.icam.i121.iot.reservicam.App;
import fr.icam.i121.iot.reservicam.R;

// Protect from minification, which would break Retrofit's reflection based parsing
@Keep
public final class Item {
	public static final int DEFAULT_ID = -1;

	@IntRange(from = DEFAULT_ID) public int id = DEFAULT_ID;
	public String name, supplier, location, state;
	public String documentation, numeric_documentation;
	public int bought, amortization, cost, maintenance;

	@Nullable public List<Borrow> borrowings;

	public Item() {
		name = App.instance.getString(R.string.new_item_name);
	}

	@NonNull
	@Override
	public String toString() {
		return String.format("(%d) %s", id, name);
	}
}
