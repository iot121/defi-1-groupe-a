package fr.icam.i121.iot.reservicam;


import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.annotation.UiThread;

import com.google.android.material.snackbar.Snackbar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.internal.EverythingIsNonNull;

public abstract class DefaultCallback<T> implements Callback<T> {
	private final View view;
	@StringRes private final int failPrefix;
	private final int snackDuration;

	protected DefaultCallback(@NonNull View view) {
		this(view, R.string.error_fail);
	}

	protected DefaultCallback(@NonNull View view, @StringRes int failPrefix) {
		this(view, failPrefix, Snackbar.LENGTH_LONG);
	}

	protected DefaultCallback(@NonNull View view, @StringRes int failPrefix, int snackDuration) {
		this.view = view;
		this.failPrefix = failPrefix;
		this.snackDuration = snackDuration;
	}

	@Override
	@EverythingIsNonNull
	public final void onResponse(Call<T> call, Response<T> response) {
		if (response.isSuccessful())
			onSuccess(call, response);
		else onError(call, response);
		onComplete();
	}

	/**
	 * In this method {@link Response<T>.body} is known to be not null
	 **/
	@EverythingIsNonNull
	protected abstract void onSuccess(Call<T> call, Response<T> response);

	/**
	 * In this method {@link Response<T>#errorbody} is known to be not null
	 */
	@EverythingIsNonNull
	@UiThread
	protected void onError(Call<T> call, Response<T> response) {
		String msg = String.format("%s : (%d) %s", App.instance.getString(failPrefix), response.code(), response.message());
		Snackbar.make(view, msg, snackDuration).show();
	}

	@Override
	@EverythingIsNonNull
	@UiThread
	public final void onFailure(Call<T> call, Throwable t) {
		String msg = String.format("%s : %s", App.instance.getString(failPrefix), t.getLocalizedMessage());
		Snackbar.make(view, msg, snackDuration).show();
		onComplete();
	}

	/**
	 * Called after any other listener, similary to try-catch's finally
	 */
	protected void onComplete() {
	}
}
