package fr.icam.i121.iot.reservicam;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;

import java.util.concurrent.ExecutionException;

public class GoogleAuth {
	private final static String GAPI_CLIENT_ID = "779196148004-k6bh87vgfj5jgrbif7tal63tdh9i5d3f.apps.googleusercontent.com";

	private final static GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
			.requestEmail()
			.requestScopes(new Scope(Scopes.OPEN_ID))
			.requestIdToken(GAPI_CLIENT_ID)
			.build();

	private final GoogleSignInClient client;
	@Nullable private GoogleSignInAccount account;

	public final static int RC_SIGN_IN = 100;

	public GoogleAuth(Context context) {
		client = GoogleSignIn.getClient(context, gso);
		account = GoogleSignIn.getLastSignedInAccount(context);
	}

	public void signIn(@NonNull Activity activity) {
		Intent signInIntent = client.getSignInIntent();
		activity.startActivityForResult(signInIntent, RC_SIGN_IN);
	}

	public int onSignInResult(Intent data) {
		try {
			account = GoogleSignIn.getSignedInAccountFromIntent(data).getResult(ApiException.class);
			return CommonStatusCodes.SUCCESS;
		} catch (ApiException e) {
			return e.getStatusCode();
		}
	}

	public Task<Void> signOut() {
		return client.signOut()
				.addOnSuccessListener(task -> account = null);
	}

	public boolean isConnected() {
		return account != null;
	}

	public String getDisplayName() {
		return account == null ? null : account.getDisplayName();
	}

	private Task<GoogleSignInAccount> refresh() {
		return client.silentSignIn().continueWith(task -> account = task.getResult());
	}

	public Task<String> getIdTokenAsync() {
		if (account == null) {
			/*if(context == null)
				return Tasks.forException(new Exception("Not connected"));

			context.startActivity(new Intent(context, AuthActivity.class));*/
			return Tasks.forResult(null);
		}

		if (account.isExpired())
			return refresh().continueWith(task -> account.getIdToken());

		return Tasks.forResult(account.getIdToken());
	}

	public String getIdTokenSync() {
		try {
			return Tasks.await(getIdTokenAsync());
		} catch (ExecutionException | InterruptedException e) {
			e.printStackTrace();
			return null;
		}
	}
}
