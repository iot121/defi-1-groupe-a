package fr.icam.i121.iot.reservicam;

import android.app.Application;

import androidx.annotation.NonNull;

public class App extends Application {
	public final static String PREF_FAVORITES = "favorites";

	@NonNull public static GoogleAuth GoogleAuth;
	@NonNull public static App instance;

	@Override
	public void onCreate() {
		super.onCreate();

		instance = this;
		GoogleAuth = new GoogleAuth(this);
	}
}