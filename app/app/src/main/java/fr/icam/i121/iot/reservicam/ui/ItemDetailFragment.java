package fr.icam.i121.iot.reservicam.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.material.appbar.CollapsingToolbarLayout;

import fr.icam.i121.iot.reservicam.R;
import fr.icam.i121.iot.reservicam.api.Item;
import fr.icam.i121.iot.reservicam.databinding.ItemDetailBinding;

public class ItemDetailFragment extends Fragment {
	private static final String ARGUMENT_ITEM_ID = "ARGUMENT_ITEM_ID";
	private Item mItem;

	/**
	 * Needed by the fragment manager
	 */
	public ItemDetailFragment() {
	}

	@NonNull
	static ItemDetailFragment newInstance(@IntRange(from = 0) int itemID) {
		ItemDetailFragment frag = new ItemDetailFragment();

		Bundle arguments = new Bundle();
		arguments.putInt(ARGUMENT_ITEM_ID, itemID);
		frag.setArguments(arguments);

		return frag;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		int id = getArguments().getInt(ARGUMENT_ITEM_ID);
		mItem = ItemListActivity.getItem(id);

		Activity activity = getActivity();
		CollapsingToolbarLayout appBarLayout = activity.findViewById(R.id.toolbar_layout);
		if (appBarLayout != null)
			appBarLayout.setTitle(mItem.name);
	}

	public void openNumericDoc(View view) {
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mItem.numeric_documentation));
		startActivity(browserIntent);
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle instanceState) {
		ItemDetailBinding binding = ItemDetailBinding.inflate(inflater, container, false);
		if (mItem != null)
			binding.setItem(mItem);

		return binding.getRoot();
	}
}
