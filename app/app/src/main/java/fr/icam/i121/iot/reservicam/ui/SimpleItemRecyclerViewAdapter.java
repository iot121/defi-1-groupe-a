package fr.icam.i121.iot.reservicam.ui;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import fr.icam.i121.iot.reservicam.api.Item;
import fr.icam.i121.iot.reservicam.databinding.ItemListContentBinding;

class SimpleItemRecyclerViewAdapter
		extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

	private final ItemListActivity mParentActivity;
	private final List<Item> mValues;
	private ItemListContentBinding binding;

	SimpleItemRecyclerViewAdapter(ItemListActivity parent, List<Item> items) {
		mValues = items;
		mParentActivity = parent;
	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		binding = ItemListContentBinding.inflate(inflater, parent, false);
		return new ViewHolder(binding);
	}

	@Override
	public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
		Item item = mValues.get(position);
		binding.setItem(item);

		holder.itemView.setTag(item);
		holder.itemView.setOnClickListener(view -> mParentActivity.openItemDetails(((Item) view.getTag()).id));
	}

	@Override
	public int getItemCount() {
		return mValues.size();
	}

	static class ViewHolder extends RecyclerView.ViewHolder {
		final TextView mIdView;
		final TextView mContentView;

		ViewHolder(@NonNull ItemListContentBinding binding) {
			super(binding.getRoot());
			mIdView = binding.idText;
			mContentView = binding.content;
		}
	}
}
