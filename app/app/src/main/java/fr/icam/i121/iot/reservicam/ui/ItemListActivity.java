package fr.icam.i121.iot.reservicam.ui;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresPermission;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import fr.icam.i121.iot.reservicam.App;
import fr.icam.i121.iot.reservicam.DefaultCallback;
import fr.icam.i121.iot.reservicam.R;
import fr.icam.i121.iot.reservicam.api.Item;
import fr.icam.i121.iot.reservicam.api.ReservationAPI;
import fr.icam.i121.iot.reservicam.databinding.ActivityItemListBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.internal.EverythingIsNonNull;

public class ItemListActivity
		extends AppCompatActivity
		implements SwipeRefreshLayout.OnRefreshListener, SearchView.OnQueryTextListener {

	private static final List<Item> ITEMS = new ArrayList<>();
	private static final List<Item> FILTERED_ITEMS = new ArrayList<>();
	private static final Map<Integer, Item> ITEM_MAP = new HashMap<>();

	private boolean mTwoPane;

	private boolean displayFavorites = false;
	private String searchTerm = null;
	private static boolean RecyclerView_UpdateRequired = false;

	private ActivityItemListBinding binding;
	private RecyclerView recyclerView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		binding = ActivityItemListBinding.inflate(getLayoutInflater());
		setContentView(binding.getRoot());

		binding.refreshLayout.setOnRefreshListener(this);

		setSupportActionBar(binding.toolbar);
		binding.toolbar.setTitle(getTitle());

		binding.fabAdd.setOnClickListener(view -> openItemDetails(Item.DEFAULT_ID));

		// The detail container view will be present only in the
		// large-screen layouts (res/values-w900dp).
		// If this view is present, then the
		// activity should be in two-pane mode.
		mTwoPane = findViewById(R.id.item_detail_container) != null;

		recyclerView = findViewById(R.id.item_list);
		recyclerView.setAdapter(new SimpleItemRecyclerViewAdapter(this, FILTERED_ITEMS));
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (!App.GoogleAuth.isConnected())
			startActivity(new Intent(this, AuthActivity.class));
		else if (ITEMS.size() == 0)
			onRefresh();
		else if (RecyclerView_UpdateRequired) {
			binding.loadingLayout.setVisibility(View.GONE);
			onQueryTextSubmit(searchTerm);
		}
	}

	@Override
	@RequiresPermission(Manifest.permission.INTERNET)
	// Called when SwipeRefreshLayout requests a refresh
	public void onRefresh() {
		RecyclerView.Adapter adapter = recyclerView.getAdapter();
		recyclerView.removeAllViews();
		adapter.notifyItemRangeRemoved(0, FILTERED_ITEMS.size());
		ITEMS.clear();
		ITEM_MAP.clear();

		binding.loadingLayout.setVisibility(View.VISIBLE);

		Callback<List<Item>> callback = new DefaultCallback<List<Item>>(binding.getRoot(), R.string.error_fetch_failed) {
			@Override
			protected void onComplete() {
				binding.loadingLayout.setVisibility(View.GONE);
				binding.refreshLayout.setRefreshing(false);
			}

			@Override
			@EverythingIsNonNull
			public void onSuccess(Call<List<Item>> call, Response<List<Item>> response) {
				response.body().forEach(ItemListActivity::addItem);
				onQueryTextSubmit(searchTerm);
			}
		};
		ReservationAPI.api.getItems(null).enqueue(callback);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_itemlist, menu);

		final MenuItem searchItem = menu.findItem(R.id.app_bar_search);
		final SearchView searchView = (SearchView) searchItem.getActionView();
		searchView.setOnQueryTextListener(this);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(@NonNull MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
			case R.id.menu_favorites:
				displayFavorites = !displayFavorites;
				onQueryTextSubmit(searchTerm);
				break;
			case R.id.menu_scan:
				if (ITEMS.size() == 0) {
					Snackbar.make(recyclerView, R.string.msg_data_update_required, Snackbar.LENGTH_SHORT).show();
					break;
				}
				startActivity(new Intent(this, QrActivity.class));
				break;
			case R.id.menu_refresh:
				onRefresh();
				break;
			case R.id.menu_sign_out:
				App.GoogleAuth.signOut()
						.addOnSuccessListener(task -> startActivity(new Intent(this, AuthActivity.class)));
				break;
		}

		return super.onOptionsItemSelected(item);
	}

	static void addItem(@NonNull Item item) {
		RecyclerView_UpdateRequired = true;
		Item old = ITEM_MAP.get(item.id);
		ITEM_MAP.put(item.id, item);

		if (old != null)
			ITEMS.set(ITEMS.indexOf(old), item);
		else ITEMS.add(item);
	}

	static void removeItem(@IntRange(from = 0) int itemID) {
		Item item = ITEM_MAP.get(itemID);
		if (item == null)
			return;

		RecyclerView_UpdateRequired = true;
		ITEM_MAP.remove(itemID);
		ITEMS.remove(item);
	}

	public static Item getItem(@IntRange(from = 0) int id) {
		return ITEM_MAP.get(id);
	}

	/**
	 * Open item details or edit a new one if @itemID == Item.DEFAULT_ID
	 */
	public void openItemDetails(@IntRange(from = Item.DEFAULT_ID) int itemID) {
		if (mTwoPane) {
			Fragment fragment = itemID == Item.DEFAULT_ID
					? ItemEditFragment.newInstance(itemID)
					: ItemDetailFragment.newInstance(itemID);

			getSupportFragmentManager().beginTransaction()
					.replace(R.id.item_detail_container, fragment)
					.commit();
		} else startActivity(ItemDetailActivity.getStartIntent(this, itemID));
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		RecyclerView.Adapter adapter = recyclerView.getAdapter();
		if(adapter == null)
			return false;

		if (query == null) {
			adapter.notifyItemRangeRemoved(0, FILTERED_ITEMS.size());
			FILTERED_ITEMS.clear();

			if (displayFavorites) {
				SharedPreferences prefs = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
				Set<String> favoriteIds = prefs.getStringSet(App.PREF_FAVORITES, new HashSet<>());
				Stream<Item> favorites = ITEMS.stream().filter(i -> favoriteIds.contains(Integer.toString(i.id)));
				FILTERED_ITEMS.addAll(favorites.collect(Collectors.toList()));
			} else FILTERED_ITEMS.addAll(ITEMS);

			adapter.notifyItemRangeInserted(0, FILTERED_ITEMS.size());
			RecyclerView_UpdateRequired = false;

			return true;
		}

		searchTerm = query.toLowerCase();
		String[] terms = searchTerm.split(" ");

		adapter.notifyItemRangeRemoved(0, FILTERED_ITEMS.size());
		FILTERED_ITEMS.clear();

		Stream<Item> items = ITEMS.stream().filter(i -> {
			String item_text = (i.name + " " + i.supplier).toLowerCase();
			return Arrays.stream(terms).allMatch(item_text::contains);
		});

		if (displayFavorites) {
			SharedPreferences prefs = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
			List<String> favoriteIds = Arrays.asList(prefs.getString(App.PREF_FAVORITES, "").split(","));
			items = items.filter(i -> favoriteIds.contains(Integer.toString(i.id)));
		}

		FILTERED_ITEMS.addAll(items.collect(Collectors.toList()));
		adapter.notifyItemRangeInserted(0, FILTERED_ITEMS.size());

		return true;
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		return onQueryTextSubmit(newText);
	}

}
