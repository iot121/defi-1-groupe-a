package fr.icam.i121.iot.reservicam.ui;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresPermission;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.ObservableBoolean;

import com.google.android.material.snackbar.Snackbar;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fr.icam.i121.iot.reservicam.App;
import fr.icam.i121.iot.reservicam.DefaultCallback;
import fr.icam.i121.iot.reservicam.LambdaCallback;
import fr.icam.i121.iot.reservicam.R;
import fr.icam.i121.iot.reservicam.api.Borrow;
import fr.icam.i121.iot.reservicam.api.Item;
import fr.icam.i121.iot.reservicam.api.ReservationAPI;
import fr.icam.i121.iot.reservicam.databinding.ActivityItemDetailBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.internal.EverythingIsNonNull;

/**
 * An activity representing a single Item detail screen. This
 * activity is only used on narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link ItemListActivity}.
 * The ItemDetailsFragment is initially displayed for that item.
 * It is replaced by the ItemEditFragment if the edit button is pressed or if itemID is Item.DEFAULT_ID.
 * In the latter case, a new Item will be created with default values and shown for edition
 */
public class ItemDetailActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
	private static final String ARGUMENT_ITEM_ID = "ARGUMENT_ITEM_ID";
	private final static String TAG_BORROW_START = "TAG_BORROW_START";
	private final static String TAG_BORROW_END = "TAG_BORROW_END";

	@IntRange(from = Item.DEFAULT_ID)
	private int itemID;
	private ItemEditFragment EditFragment;
	private Calendar BorrowStart;

	private ActivityItemDetailBinding binding;

	private final ObservableBoolean Editing = new ObservableBoolean();
	private final ObservableBoolean Uploading = new ObservableBoolean();

	@NonNull
	static Intent getStartIntent(Context context, @IntRange(from = Item.DEFAULT_ID) int itemID) {
		Intent intent = new Intent(context, ItemDetailActivity.class);
		intent.putExtra(ARGUMENT_ITEM_ID, itemID);
		return intent;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		binding = ActivityItemDetailBinding.inflate(getLayoutInflater());
		setContentView(binding.getRoot());

		itemID = getIntent().getIntExtra(ARGUMENT_ITEM_ID, Item.DEFAULT_ID);

		setSupportActionBar(binding.detailToolbar);

		binding.fabEdit.setOnClickListener(view -> displayEditUI());
		binding.setEditing(Editing);
		binding.setUploading(Uploading);
		binding.setItem(ItemListActivity.getItem(itemID));

		// Show the Up button in the action bar.
		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null)
			actionBar.setDisplayHomeAsUpEnabled(true);

		// savedInstanceState is non-null when there is fragment state
		// saved from previous configurations of this activity
		// (e.g. when rotating the screen from portrait to landscape).
		// In this case, the fragment will automatically be re-added
		// to its container so we don't need to manually add it.
		// For more information, see the Fragments API guide at:
		//
		// http://developer.android.com/guide/components/fragments.html
		//
		if (savedInstanceState == null) {
			if (itemID == Item.DEFAULT_ID) displayEditUI();
			else displayDetailsUI();
		}
	}

	@Override
	public boolean onOptionsItemSelected(@NonNull MenuItem item) {
		int id = item.getItemId();
		if (id == android.R.id.home) {
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			navigateUpTo(new Intent(this, ItemListActivity.class));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void displayDetailsUI() {
		Editing.set(false);

		ItemDetailFragment fragment = ItemDetailFragment.newInstance(itemID);
		getSupportFragmentManager().beginTransaction()
				.add(R.id.item_detail_container, fragment)
				.commit();
	}

	private void displayEditUI() {
		Editing.set(true);

		EditFragment = ItemEditFragment.newInstance(itemID);
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.item_detail_container, EditFragment)
				.commit();
	}

	@RequiresPermission(Manifest.permission.INTERNET)
	public void deleteItem(View view) {
		String msg = String.format("%s (%d) %s ?", getString(R.string.action_delete), itemID, ItemListActivity.getItem(itemID).name);

		Callback<Void> callback = new LambdaCallback<>(view, R.string.error_delete_failed, (call, response) -> {
			ItemListActivity.removeItem(itemID);
			Snackbar.make(view, R.string.msg_deleted, Snackbar.LENGTH_LONG).show();
			onBackPressed();
		});

		new AlertDialog.Builder(this)
				.setTitle(R.string.action_delete)
				.setMessage(msg)
				.setIcon(android.R.drawable.ic_dialog_alert)
				.setCancelable(true)
				.setPositiveButton(android.R.string.yes,
						(dialog, button) -> ReservationAPI.api.deleteItem(itemID).enqueue(callback))
				.show();
	}

	@RequiresPermission(Manifest.permission.INTERNET)
	public void borrowItem(View view) {
		Calendar now = Calendar.getInstance();
		DatePickerDialog dialog = DatePickerDialog.newInstance(
				this,
				now.get(Calendar.YEAR), // Initial year selection
				now.get(Calendar.MONTH), // Initial month selection
				now.get(Calendar.DAY_OF_MONTH) // Inital day selection
		);
		int accent = ContextCompat.getColor(this, R.color.colorAccent);
		int primary = ContextCompat.getColor(this, R.color.colorPrimary);
		dialog.setAccentColor(accent);
		dialog.setOkColor(primary);
		dialog.setMinDate(now);
		dialog.setTitle(getString(R.string.title_borrow_from));

		Callback<Item> callback = new LambdaCallback<>(view, R.string.error_fetch_failed, (call, response) -> {
			Set<Calendar> disabledDays = new HashSet<>();
			for (Borrow b : response.body().borrowings) {
				Calendar day = Calendar.getInstance();
				Calendar end = Calendar.getInstance();
				day.setTime(b.start);
				end.setTime(b.end);

				for (day.setTime(b.start); end.after(day); day.add(Calendar.DATE, 1)) {
					// Clone day
					Calendar date = Calendar.getInstance();
					date.setTime(day.getTime());
					disabledDays.add(date);
				}
				disabledDays.add(end);
			}
			dialog.setDisabledDays(disabledDays.toArray(new Calendar[0]));
			dialog.show(getSupportFragmentManager(), TAG_BORROW_START);
		});
		ReservationAPI.api.getItem(itemID).enqueue(callback);
	}

	public void favoriteItem(View view) {
		SharedPreferences prefs = getSharedPreferences(getString(R.string.app_name), Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();

		// Retrieved Set must not be modified, feed it to a list
		List<String> favoriteIds = new ArrayList<>(prefs.getStringSet(App.PREF_FAVORITES, new HashSet<>()));
		if (!favoriteIds.remove(Integer.toString(itemID)))
			favoriteIds.add(Integer.toString(itemID));

		editor.putStringSet(App.PREF_FAVORITES, new HashSet<>(favoriteIds));
		editor.apply();
	}

	@RequiresPermission(Manifest.permission.INTERNET)
	public void saveItem(View view) {
		if (EditFragment == null) {
			System.err.println("saveItem should not be called without an initialized ItemEditFragment");
			return;
		}

		Uploading.set(true);

		Item item = EditFragment.getInput();

		Call req;
		if (item.id == Item.DEFAULT_ID)
			req = ReservationAPI.api.createItem(item);
		else req = ReservationAPI.api.updateItem(item.id, item);


		req.enqueue(new DefaultCallback(binding.getRoot(), R.string.error_save_failed) {
			@Override
			@EverythingIsNonNull
			public void onSuccess(Call call, Response response) {
				Object body = response.body();
				if (body instanceof Integer)
					item.id = (int) body;
				ItemListActivity.addItem(item);
				onBackPressed();
			}

			@Override
			protected void onComplete() {
				// Actually only used on error and failure, but the activity will exit on success
				// Making it not an issue
				Uploading.set(false);
			}
		});
	}

	@Override
	@RequiresPermission(Manifest.permission.INTERNET)
	public void onDateSet(@NonNull DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
		Calendar date = Calendar.getInstance();
		date.set(year, monthOfYear, dayOfMonth);

		String tag = view.getTag();
		if (tag == null)
			return;

		switch (tag) {
			case TAG_BORROW_START:
				BorrowStart = date;

				int accent = ContextCompat.getColor(this, R.color.colorAccent);
				int primary = ContextCompat.getColor(this, R.color.colorPrimary);
				DatePickerDialog dialog = DatePickerDialog.newInstance(this, year, monthOfYear, dayOfMonth);
				dialog.setMinDate(BorrowStart);
				dialog.setTitle(getString(R.string.title_borrow_to));
				dialog.setAccentColor(accent);
				dialog.setOkColor(primary);
				Calendar[] disabledDays = view.getDisabledDays();
				dialog.setDisabledDays(disabledDays == null ? new Calendar[0] : disabledDays);
				dialog.show(getSupportFragmentManager(), TAG_BORROW_END);
				break;

			case TAG_BORROW_END:
				Borrow borrow = new Borrow(BorrowStart.getTime(), date.getTime(), null);
				Callback<Void> callback = new LambdaCallback<>(binding.getRoot(),
						(call, response) -> Snackbar.make(binding.getRoot(), R.string.msg_borrowed, Snackbar.LENGTH_LONG).show()
				);
				ReservationAPI.api.borrowItem(itemID, borrow).enqueue(callback);
		}
	}
}
