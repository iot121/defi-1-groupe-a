package fr.icam.i121.iot.reservicam.api;

import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Date;

// Protect from minification, which would break Retrofit's reflection based parsing
@Keep
public final class Borrow {
	public Date start, end;
	@Nullable public String borrower;

	public Borrow() {
	}

	public Borrow(@NonNull Date start, @NonNull Date end, @Nullable String borrower) {
		this.start = start;
		this.end = end;
		this.borrower = borrower;
	}
}
