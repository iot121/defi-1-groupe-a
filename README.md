# Défi 1 "Systeme de réservation"

## Equipe 2021-A
 * Camille Fichot
 * Etienne Perret
 * Julien Poupon
 * Mathis Ribet

 ## Tâches
[Document Rocheteau](https://docs.google.com/document/d/1rMYoMANbhLHwMN4gRcMUHBgakACBzwkfjbw9ruK3V1c)
 * Serveur
   * [ ] Base de données **WIP**
   * [ ] Documents
   * Endpoints
     * [X] Consultation
     * [X] Recherche
     * [X] Authentification
     * [X] Reservation
     * [X] Gestion des équipements
 * Interface Web
   * [ ] Consultation
   * [ ] Recherche
   * [ ] Authentification **WIP**
   * [ ] Reservation
   * [ ] Gestion des equipements
 * Interface Android
   * [X] QR code
   * [X] Consultation
   * [X] Recherche
   * [X] Authentification
   * [X] Reservation
   * [X] Gestion des équipements

## Structure
Une API type REST interface la base de données contenant les informations des objets empruntables
et la liste des réservations.

Les utilisateurs sont authentifiés via Google Sign In restreint à l'organisation Icam.
Tous les membres de l'équipe pédagogique (domaine mail icam.fr) peuvent consulter et réserver les objets.
Les administrateurs (stockés dans la BDD) peuvent modifier les informations des objets.

2 interfaces utilisateurs exploitent l'API : une page web et une application Android.
Les 2 interfaces supportent l'ensemble des fonctionnalités.

## Arborescence
* L'interface web (servie par Apache) est conçue en statique dans le dossier [drafts_web](drafts_web)
* Les fichiers PHP générant cette interface, ainsi que les points d'accès mobile sont dans le dossier [serveur](serveur)
* Le code de génération de la base de données se trouve dans le dossier [database](database)
* Le projet Android Studio pour l'application mobile est dans le dossier [app](app)
