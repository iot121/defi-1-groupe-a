ARG JDK_VERSION=8

FROM adoptopenjdk/openjdk$JDK_VERSION:alpine

ARG ANDROID_COMPILE_SDK=29
ARG ANDROID_BUILD_TOOLS=29.0.3
ARG ANDROID_SDK_TOOLS=6514223

ENV ANDROID_HOME=/root
ENV PATH="${PATH}:${ANDROID_HOME}/cmdline-tools/tools/bin"
ENV GRADLE_OPTS="-Dorg.gradle.daemon=false"

RUN apk add --no-cache unzip wget ca-certificates \
  && rm -rf /var/cache/apk/*

RUN wget --quiet --output-document=android-sdk.zip https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_SDK_TOOLS}_latest.zip \
  && mkdir ${ANDROID_HOME}/cmdline-tools \
  && unzip -qq -d ${ANDROID_HOME}/cmdline-tools android-sdk.zip \
  && rm -v android-sdk.zip

RUN echo y | sdkmanager --verbose "platform-tools"
RUN echo y | sdkmanager --verbose "platforms;android-${ANDROID_COMPILE_SDK}"
RUN echo y | sdkmanager --verbose "build-tools;${ANDROID_BUILD_TOOLS}" \
  && echo y| sdkmanager --verbose --uninstall emulator

RUN yes | sdkmanager --licenses
